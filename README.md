### 注意事项
1、自定义组件
Taro组件初始化与React组件初始化有些不同，[constructor和render会提前调用](https://nervjs.github.io/taro/docs/best-practice.html#%E7%BB%84%E4%BB%B6%E7%9A%84-constructor-%E4%B8%8E-render-%E6%8F%90%E5%89%8D%E8%B0%83%E7%94%A8)

2、js代码必须使用单引号,否则Taro编译可能会报错

import Taro, { Component } from '@tarojs/taro'
import Index from './pages/index'
import './common/theme.scss'
import './app.scss'
import config from './common/config';
import { checkLogin } from './common/wx';

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }
let isInit = 0;
class App extends Component {

  componentDidMount () {
    isInit = 1;
  }

  componentDidShow () {
    // 小程序从后台进入前台时，需要查验用户到数据
    if (isInit) {
      checkLogin();
    }

  }

  componentDidHide () {}

  componentDidCatchError () {}

  config = {
    pages: [
      'pages/book/index',
      'pages/login/index',
      'pages/index/index',
      'pages/mine/index',
      'pages/authLogin/index',
      'pages/bookDetail/index'
    ],
    window: {
      backgroundTextStyle: 'dark',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: '#999',
      selectedColor: config.primaryColor,
      list: [{
        pagePath: 'pages/book/index',
        text: '首页',
        iconPath: 'assets/component.png',
        selectedIconPath: 'assets/component_cur.png',
      }, {
        pagePath: 'pages/mine/index',
        text: '我的',
        iconPath: 'assets/about.png',
        selectedIconPath: 'assets/about_cur.png',
      }]
    }
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Index />
    )
  }
}

Taro.render(<App />, document.getElementById('app'))

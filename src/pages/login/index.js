import Taro, { useEffect, useState } from '@tarojs/taro';
import { AtForm, AtInput, AtButton, AtTabs, AtTabsPane, AtMessage } from 'taro-ui'
import { View, Text } from '@tarojs/components';
import './index.scss';
import validate from '../../utils/dj-validate'

function Login() {
  const [ login, setLogin ] = useState({});
  // componentDidMount
  useEffect(() => {
    // console.log(props)
  }, [])
  // componentWillUnmount
  useEffect(() => {
    return () => {
      // console.log(props)
    }
  }, [])

  const userLogin = (e) => {
    const formData = e.detail.value
    const rule = [
      { name: "phone", checkType: "phoneno", required: 'required', emptyMsg: '手机号必填', errorMsg: "手机号格式错误" },
      { name: "password", checkType: "string", required: 'required', emptyMsg: '密码必填', checkRule: "6,20", errorMsg: "密码6-20位" }
    ];
    const checkRes = validate.check(formData, rule)
    if (!checkRes) {
      Taro.atMessage({
        type: 'error',
        message: validate.error
      })
      return
    }
  }

  return (
    <View className='content'>
      <AtMessage />
      <View className='login-title'>
        <View>登录</View>
      </View>
      <AtForm
        onSubmit={userLogin}>
        <AtInput
          name='phone'
          title='用户名'
          type='text'
          placeholder='用户名或手机号'
          value={login.phone}>
        </AtInput>
        <AtInput
          name='password'
          title='密码'
          type='password'
          placeholder='密码'
          value={login.password}>
        </AtInput>
        <AtButton formType='submit' type='primary'>登录</AtButton>
      </AtForm>
      <View className='login-type'>
        <View>验证码</View>
        <View>QQ</View>
        <View>微信</View>
      </View>
    </View >
  )
}

//全局样式继承 你可以关掉
Login.options = {
  addGlobalClass: true
}

Login.config = {
  navigationBarTitleText: '首页',
  enablePullDownRefresh: true,
};

export default Login;

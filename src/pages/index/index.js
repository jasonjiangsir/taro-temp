import Taro, { useState, useEffect, useCallback, usePullDownRefresh } from '@tarojs/taro'
import { View, Swiper, SwiperItem, Image } from '@tarojs/components'
import { AtSearchBar } from 'taro-ui'
import './index.scss'
import { imagePath } from '../../utils';
import request from '../../common/request';

function Index() {
  const [advertisingList, setAdvertisingList] = useState([]);
  // 获取后台数据
  const getAdvertisingList = useCallback(async (params) => {
    const result = await request({ tartget: 'advertisingList', data: params });
    if (result.success) {
      setAdvertisingList(result.models || []);
    }
    Taro.stopPullDownRefresh();
  }, []);
  // 初始化
  useEffect(() => {
    getAdvertisingList();
  }, [getAdvertisingList]);
  // 下拉刷新
  usePullDownRefresh(() => {
    getAdvertisingList();
  }, []);
  return (
    <View className='content'>
      <View className='search-wrap'>
        <AtSearchBar
          disabled
          value=''
          actionName='搜索'
        />
      </View>
      <View className='swiper'>
        <Swiper className='swiper-box' indicator-dots indicator-color='rgba(255,255,255,0.5)' indicator-active-color='#FFF' autoplay circular>
          {
            advertisingList.map((item) => {
              return (
                <SwiperItem key={item.id}>
                  <View
                    className='swiper-item'
                    data-id={item.id}
                    data-is-router={item.isRouter}
                    data-target='advertise'
                  >
                    <Image className='swiper-item' src={imagePath(item.cover)} ></Image>
                  </View>
                </SwiperItem>
              );
            })
          }
        </Swiper>
      </View>
    </View>
  );
}

Index.config = {
  navigationBarTitleText: '首页',
  enablePullDownRefresh: true,
};

export default Index;


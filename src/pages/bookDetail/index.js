import Taro, { useEffect } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import { AtAvatar, AtDivider, AtList, AtListItem } from 'taro-ui'
import { connect } from '@tarojs/redux';
import './index.scss';

const Bookdetail = props => {
  const { book, loading } = props;

  // componentDidMount
  useEffect(() => {
    console.log("props", book)
  }, [])
  // componentWillUnmount
  useEffect(() => {
    return () => {
      console.log(props)
    }
  }, [])
  return (
    <View className="index-page" style="background-color:white">
      <View style="padding-left:20rpx;padding-top:20rpx;">
        <AtAvatar circle text={book.contactUserDetail.name}></AtAvatar>
      </View>
      <View style="padding-left:30rpx;padding-top:20rpx;">
        <Text>{book.contactUserDetail.name}</Text>
      </View>
      <View style="padding-top:20rpx;">
        <AtList>
          <AtListItem
            note='手机 | 上海 电信'
            title='13322224444'
            extraText='电话 短信'
          /> 
        <AtListItem
            note='手机 | 上海 电信'
            title='13322224444'
            extraText='电话 短信'
          />
          </AtList>
      </View>
    </View>
  )
}
Bookdetail.config = {
  navigationBarTitleText: 'bookDetail'
}
//全局样式继承 你可以关掉
Bookdetail.options = {
  addGlobalClass: true
}

const mapStateToProps = ({ loading, book }) => {
  return {
    loading,
    book,
  };
};

export default connect(mapStateToProps)(Bookdetail)

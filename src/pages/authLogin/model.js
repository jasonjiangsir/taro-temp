import * as authLoginApi from './service';
import Taro from '@tarojs/taro';

export default {
  namespace: 'authLogin',
  state: {
    keai: '测试数据666',
    loading: false,
    token: '',
  },

  effects: {
    * effectsDemo(_, { call, put }) {
      const { status, data } = yield call(authLoginApi.demo, {});
      if (status === 'ok') {
        yield put({
          type: 'updateState',
          payload: {
            topData: data,
          }
        });
      }
    },
    * login({ payload }, { call, put }) {
      const { success, model, msg } = yield call(authLoginApi.login, payload)
      if (success === 1) {
        Taro.setStorage({ key: 'token', data: model })
        yield put({
          type: 'updateState',
          payload: {
            token: model
          }
        })
        Taro.switchTab({ url: '/pages/book/index' })
      } else {
        Taro.atMessage({
          type: 'error',
          message: msg
        })
      }
    },
    * setData({ payload }, { call, put }) {
      yield put({
        type: 'updateState',
        payload,
      })
    }
  },

  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
  },

};

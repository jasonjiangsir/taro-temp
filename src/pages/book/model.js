import * as bookApi from './service';

const setState = {
  age: 18,
  keai: '测试数据666',
  inputValue: '',
  searchKey: '',
  contactUserList: [{
    title: 'A',
    key: 'A',
    items: [
      {
        'name': '阿坝'
        // 此处可加其他业务字段
      },
      {
        'name': '阿拉善'
      }]
  },
  {
    title: 'B',
    key: 'B',
    items: [
      {
        'name': '北京'
      },
      {
        'name': '保定'
      }]
  }
  ],
  contactUserDetail:{}
}

export default {
  namespace: 'book',
  state: { ...setState },

  effects: {
    * initData(_, { call, put }) {
      console.log("init data")
      yield put({
        type: 'updateState',
        payload: {
          keai: '测试数据666'
        }
      })
    },
    * effectsDemo(_, { call, put }) {
      const { status, data } = yield call(bookApi.demo, {});
      if (status === 'ok') {
        yield put({
          type: 'updateState',
          payload: {
            topData: data,
          }
        });
      }
    },
    * setData({ payload }, { call, put }) {
      yield put({
        type: 'updateState',
        payload,
      })
    }
  },

  reducers: {
    updateState(state, { payload }) {
      return { ...state, ...payload };
    },
  },

};

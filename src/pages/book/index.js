import Taro, { useEffect, navigateTo } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import { AtIndexes, AtSearchBar, AtFab } from 'taro-ui'
import { connect } from '@tarojs/redux';
import './index.scss';

const Book = props => {
  const { book, loading, dispatch } = props;
  // componentDidMount
  useEffect(() => {
    try {
      Taro.getSetting({
        success: function (res) {
          console.log("auth", res)
          if (!res.authSetting["scope.userInfo"]) {
            Taro.reLaunch({
              url: '/pages/authLogin/index'
            })
          }
        }
      })
    } catch (e) {
      // Do something when catch error
    }
    console.log("init contactUserList")
    dispatch({ type: 'index/initData' })
  }, [])
  // componentWillUnmount
  useEffect(() => {
    return () => {
      console.log(props)
    }
  }, [])
  const handleClick = () => {
    dispatch({
      type: 'book/clear'
    })
  }
  const onSearchClick = () => {

  }
  const onSearchValueClick = () => {

  }
  const onClick = (e) => {
    dispatch({
      type: 'book/setData',
      payload: {
        contactUserDetail: e
      }
    })
    Taro.navigateTo({
      url: '/pages/bookDetail/index?id=' + e.id,
      success: function (res) {
        console.log(book.contactUserDetail)
      }
    })
  }
  const onAddClick = ()=>{

  }
  return (
    <View className="index-page" style="height:100vh">
      <AtIndexes
        list={book.contactUserList}
        onClick={onClick}
        onScrollIntoView={fn => { this.scrollIntoView = fn }}
      >
        <View className='custom-area'>
          <AtSearchBar placeholder='搜索姓名昵称'
            value={book.searchKey}
            onClick={onSearchValueClick}
            onActionClick={onSearchClick} />
        </View>
      </AtIndexes>
      <View style="position:absolute;right:60rpx;bottom:40rpx;">
      <AtFab onClick={onAddClick} size="small">
        <Text className='at-fab__icon at-icon at-icon-add'></Text>
      </AtFab>
      </View>
    </View>
  )
}
Book.config = {
  navigationBarTitleText: 'book'
}
//全局样式继承 你可以关掉
Book.options = {
  addGlobalClass: true
}

const mapStateToProps = ({ loading, book }) => {
  return {
    loading,
    book,
  };
};

export default connect(mapStateToProps)(Book)

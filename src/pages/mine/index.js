import Taro, { useEffect, useState } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import './index.scss';

function Mine() {
  const [mine] = useState({});
  // componentDidMount
  useEffect(() => {
    // console.log(props)
  }, [])
  // componentWillUnmount
  useEffect(() => {
    return () => {
      // console.log(props)
    }
  }, [])
  return (
    <View className="index-page">
      <Text>正如你所见这是你的index页面</Text>
    </View>
  )
}
//全局样式继承 你可以关掉
Mine.options = {
  addGlobalClass: true
}

Mine.config = {
  navigationBarTitleText: '首页',
  enablePullDownRefresh: true,
};

export default Mine;

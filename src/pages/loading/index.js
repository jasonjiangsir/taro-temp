import Taro, { useEffect } from '@tarojs/taro'
import { View } from '@tarojs/components';
import { checkLogin } from '../../common/wx';

function Loading() {

  useEffect(() => {
    // 在此校验登录状态
    checkLogin(true, true);
  }, []);
  return <View />;
}

Loading.config = {
  navigationBarTitleText: '加载中...',
}

export default Loading;

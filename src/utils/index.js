import config from '../common/config';

// 查看微信头像等非本地服务图像
export function headimgHD(imageUrl) {
  if (imageUrl.indexOf('vi_32') < 0) {
    return imageUrl;
  }
  imageUrl = imageUrl.split('/'); //把头像的路径切成数组
  //把大小数值为 46 || 64 || 96 || 132 的转换为0
  if (
    imageUrl[imageUrl.length - 1] &&
    (imageUrl[imageUrl.length - 1] == 46 ||
      imageUrl[imageUrl.length - 1] == 64 ||
      imageUrl[imageUrl.length - 1] == 96 ||
      imageUrl[imageUrl.length - 1] == 132)
  ) {
    imageUrl[imageUrl.length - 1] = 0;
  }
  imageUrl = imageUrl.join('/'); //重新拼接为字符串
  return imageUrl;
};
// 替换图片链接
export function imagePath(path) {
  if (!path) {
      return;
  }
  if (path.indexOf('http') > -1) {
      return path;
  }
  return config.apiPrefix + path;
};
// 按长度截取字符串长度
export function substr(str, length) {
  if (!str || !length) {
    return str;
  }

  return str.length > length ? str.substring(0, length) + '...' : str;
};
// 转换时间格式
export function	formatTimeZh(date) {
  if (!date) {
    return;
  }
  date = new Date(date);

  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var hour = date.getHours()
  var minute = date.getMinutes()

  var dateList = [year, month, day].map(this.formatNumber);
  return dateList[0] + '年' + dateList[1] + "月" + dateList[2] + '日' + ' ' + [hour, minute].map(this.formatNumber).join(':')
}
// 转换单位数为双位数：9 -> 09
export function	formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
};
// 延迟函数
export function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

